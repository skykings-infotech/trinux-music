@servers(['web' => '-A -p 143 -l root 159.89.160.53'])

@task('list', ['on' => 'web'])
    ls -l
@endtask

@setup
    $repository = 'git@gitlab.com:trinux-dev/trinux-music.git';
    $release_dir = '/home/rishabh/public_html/trinux-music';
    $beta_release_dir = '/home/rishabh/public_html/trinux-music-beta';
@endsetup

@story('deploy')
    clone_repository
    run_composer
    run_yarn
@endstory

@task('clone_repository')
    echo 'Cloning repository'
    rm -rf {{ $release_dir }}
    mkdir -p {{ $release_dir }}
    git clone {{ $repository }} {{ $release_dir }}
@endtask

@task('run_composer')
    echo "Starting deployment - composer"
    cd {{ $release_dir }}
    composer install --prefer-dist --no-scripts -q -o
@endtask

@task('run_yarn')
    echo "Starting deployment - yarn"
    cd {{ $release_dir }}
    sudo chown -R www-data:www-data /var/www
@endtask

@story('beta')
    beta_upload
    beta_run_composer
@endstory

@task('beta_upload')
    echo 'Cloning'
    rm -rf {{ $beta_release_dir }}
    mkdir -p {{ $beta_release_dir }}
    git clone --single-branch -b dev {{ $repository }} {{ $beta_release_dir }}
@endtask

@task('beta_run_composer')
    echo "Starting deployment - composer"
    cd {{ $beta_release_dir }}
    composer install --prefer-dist --no-scripts -q -o
    mv {{ $beta_release_dir }}/.env.server {{ $beta_release_dir }}/.env
    sudo chown -R rishabh:www-data {{ $beta_release_dir }}
    sudo chmod 775 -R {{ $beta_release_dir }}
@endtask
