<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class Album extends Model
{
  protected $table = 'albums';

  /**
   * The attributes that are mass assignable.
   *
   * @var array
   */
  protected $fillable = [
    'title',
    'image',
    'dsc',
    'slug',
    'keywords'
  ];

  protected $hidden = [
    'id',
    'created_at',
    'updated_at'
  ];

  protected $dates = [
    'created_at',
    'updated_at'
  ];

  protected $appends = [
    'key'
  ];

  /**
   * Get the Album's id.
   *
   * @param  string  $value
   * @return string
   */
  public function getKeyAttribute($value)
  {
    return Hashids::encode( $this->attributes['id'] );
  }

  public function tracks() {
    return $this->belongsTo( 'App\Track', 'id', 'album_id' );
  }

  public function artists() {
    return $this->hasManyThrough(
      'App\Artist',
      'App\Track',
      'album_id',
      'id',
      'id',
      'artist_id'
    );
  }
}
