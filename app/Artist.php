<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class Artist extends Model
{
  protected $table = 'artists';

  protected $hidden = [
    'id',
    'created_at',
    'updated_at'
  ];

  protected $dates = [
    'created_at',
    'updated_at'
  ];

  protected $appends = [
    'key'
  ];

  /**
   * Get the Artist's id.
   *
   * @param  string  $value
   * @return string
   */
  public function getKeyAttribute($value)
  {
    return Hashids::encode( $this->attributes['id'] );
  }

  public function albums() {
    return $this->hasManyThrough(
      'App\Album',
      'App\Track',
      'artist_id',
      'id',
      'id',
      'album_id'
    );
  }

  public function tracks() {
    return $this->hasMany(
      'App\Track',
      'id'
    );
  }
}
