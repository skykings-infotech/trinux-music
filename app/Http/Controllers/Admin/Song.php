<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class Song extends Controller
{
  public function __construct()
  {

  }

  public function upload( Request $request )
  {
    $request->validate([
      'song' => 'required',
      'song_data.title' => 'required',
      'song_data.track' => 'required',
      'song_data.album' => 'required',
      'song_data.artist' => 'required',
      'song_data.album_artist' => 'required',
      'song_data.genre' => 'required',
      'song_data.copyright' => 'required',
    ]);
  }
}
