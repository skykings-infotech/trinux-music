<?php

namespace App\Http\Controllers;

use App\Album;
use Illuminate\Http\Request;
use Vinkla\Hashids\Facades\Hashids;

class AlbumsController extends Controller
{
  public function __construct() {

  }

  public function getAll( Request $request ) {
    $limit = (int) $request->input( 'limit', 25 );
    $page = (int) $request->input( 'page', 1 );
    $offset = $limit * ( $page - 1 );
    $albums = Album::withCount('tracks')->offset( $offset )->limit( $limit )->get();

    $total_albums = Album::count();

    return response()->json( [
      'pagination' => [
        'current' => $page,
        'next' => $page + 1,
        'offset' => $offset + 1,
        'total' => ceil( $total_albums / $limit ),
        'limit' => $limit,
        'total_albums' => $total_albums
      ],
      'albums' => $albums
    ] );
  }

  public function get( Request $request, $id ) {
    return Album::where( 'id', '=', Hashids::decode( $id ) )->orWhere( 'slug', '=', $id )->first();
  }

  public function delete( Request $request, $id ) {
    return Album::destroy( Hashids::decode( $id ) );
  }
}
