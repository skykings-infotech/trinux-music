<?php

namespace App\Http\Controllers;

use App\Artist;
use Illuminate\Http\Request;
use Vinkla\Hashids\Facades\Hashids;

class ArtistsController extends Controller
{
  public function __construct() {

  }

  public function getAll( Request $request ) {
    $limit = (int) $request->input( 'limit', 25 );
    $page = (int) $request->input( 'page', 1 );
    $offset = $limit * ( $page - 1 );
    $albums = Artist::with('albums', 'tracks')->offset( $offset )->limit( $limit )->get();

    $total_albums = Artist::count();

    return response()->json( [
      'pagination' => [
        'current' => $page,
        'next' => $page + 1,
        'offset' => $offset + 1,
        'total' => ceil( $total_albums / $limit ),
        'limit' => $limit,
        'total_artists' => $total_albums
      ],
      'artists' => $albums
    ] );
  }

  public function get( Request $request, $id ) {
    return Artist::where( 'id', '=', Hashids::decode( $id ) )->orWhere( 'slug', '=', $id )->first();
  }

  public function delete( Request $request, $id ) {
    return Artist::destroy( Hashids::decode( $id ) );
  }
}
