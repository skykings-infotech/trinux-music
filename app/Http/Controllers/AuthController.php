<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\URL;
use Vinkla\Hashids\Facades\Hashids;

class AuthController extends Controller
{

  public function __construct()
  {

  }

  public function login( Request $request ) {
    $auth = false;
    $credentials = $request->only('email', 'password');

    if (Auth::attempt($credentials, $request->has('remember'))) {
      $auth = true; // Success
    }

    if ($request->ajax()) {
      if ( !$auth ) {
        return response()->json([
          'status' => false
        ]);
      }

      return response()->json([
        'status' => true,
        'user' => [
          'id' => Hashids::encode( Auth::user()->id ),
          'name' => Auth::user()->name,
          'email' => Auth::user()->email,
        ]
      ]);
    } else {
      return redirect()->intended(URL::route('dashboard'));
    }
    return redirect(URL::route('login_page'));
  }

  public function user() {
    if($user = Auth::user())
    {
      return response()->json([
        'status' => true,
        'user' => [
          'id' => Hashids::encode( $user->id ),
          'name' => $user->name,
          'email' => $user->email
        ]
      ]);
    }

    return response()->json([
      'status' => false
    ]);
  }

  public function logout() {
    Auth::logout();
    if ( !Auth::check() ) {
      return response()->json([
        'status' => true
      ]);
    } else {
      return response()->json([
        'status' => false
      ]);
    }
  }

}
