<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Vinkla\Hashids\Facades\Hashids;

class Track extends Model
{
  protected $table = 'tracks';

  protected $hidden = [
    'id',
    'created_at',
    'updated_at'
  ];

  protected $dates = [
    'created_at',
    'updated_at'
  ];

  protected $appends = [
    'key'
  ];

  /**
   * Get the Song's id.
   *
   * @param  string  $value
   * @return string
   */
  public function getKeyAttribute($value)
  {
    return Hashids::encode( $this->attributes['id'] );
  }

  public function album() {
    return $this->hasMany( 'App\Album' );
  }

  public function artists() {
    return $this->belongsToMany( 'App\Artist' );
  }
}
