<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Labels extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('labels', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->string('name');
           $table->text('dsc');
           $table->string('keywords');
           $table->string('image');
           $table->string('slug')->unique();
           $table->timestamps();
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('labels');
     }
}
