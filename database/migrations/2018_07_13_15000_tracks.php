<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Tracks extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
       Schema::create('tracks', function (Blueprint $table) {
           $table->bigIncrements('id');
           $table->string('title');
           $table->string('slug')->unique();
           $table->integer('duration');
           $table->string('keywords');
           $table->string('image');
           $table->unsignedBigInteger('album_id');
           $table->unsignedBigInteger('artist_id');
           $table->unsignedBigInteger('label_id');
           $table->string('aws_object_key');
           $table->timestamps();
       });

       Schema::table('tracks', function (Blueprint $table) {
           $table->foreign('album_id')->references('id')->on('albums');
           $table->foreign('artist_id')->references('id')->on('artists');
           $table->foreign('label_id')->references('id')->on('labels');
       });
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
       Schema::dropIfExists('tracks');
     }
}
