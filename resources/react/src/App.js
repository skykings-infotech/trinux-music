import React, { Component } from 'react';
import { Provider } from 'react-redux';
import PublicRoutes from './router';
import {store, history} from './redux/store';
import './App.scss';

class App extends Component {
  render() {
    return (
      <Provider store={store}>
        <PublicRoutes history={history} />
      </Provider>
    );
  }
}

export default App;
