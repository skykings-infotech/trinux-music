import Dashboard from './pages/Backend/Dashboard';
import Artists from './pages/Backend/Artists';
import Albums from './pages/Backend/Albums';
import AlbumCreate from './pages/Backend/AlbumCreate';

export {
  Dashboard,
  Artists,
  Albums,
  AlbumCreate
}
