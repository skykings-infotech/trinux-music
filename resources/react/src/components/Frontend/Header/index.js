import React, { Component } from 'react';
import {
  Navbar
} from '../../../components';

export default class Header extends Component {
  render() {
    return (
      <div id="header">
        <Navbar />
      </div>
    );
  }
}
