import axios from 'axios';

export default axios.create({
  baseURL: '/ajax/',
  timeout: 1000,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'X-CSRF-TOKEN': document.querySelector('meta[name="csrf-token"]').content
  }
});
