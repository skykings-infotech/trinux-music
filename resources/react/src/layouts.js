import Frontend from './layouts/Frontend';
import Backend from './layouts/Backend';

export {
  Frontend,
  Backend
}
