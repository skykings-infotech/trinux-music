import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import { Popover, Menu, Icon, Avatar } from 'antd';
import { logout } from '../../../../redux/Auth/action';

class ToolbarNotifications extends Component {

  state = {
    visible: false
  }

  render() {

    const content = (
      <div className="popover-custom--wrapper">
        <div className="popover-header">
          <h3>Notifications</h3>
        </div>
      </div>
    );

    return (
      <Popover
        content={content}
        trigger="click"
        placement="bottomLeft" >
        <li className="nav-item">
          <Icon type="bell" />
        </li>
      </Popover>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return { actions: bindActionCreators({
    logout
  }, dispatch) };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ToolbarNotifications);
