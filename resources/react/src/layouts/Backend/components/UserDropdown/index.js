import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import { Popover, Badge, Menu, Icon, Avatar } from 'antd';
import { logout } from '../../../../redux/Auth/action';

class UserDropdown extends Component {

  render() {

    const content = (
      <div className="popover-custom--wrapper">
        <ul className="popover-custom-menu">
          <Link to="/panel/account/settings">
            <li className="popover-custom-menu-item">Settings</li>
          </Link>
          <li
            className="popover-custom-menu-item"
            onClick={( event ) => {
              this.props.actions.logout();
            }} >Logout</li>
        </ul>
      </div>
    );

    return (
      <Popover
        content={content}
        trigger="click"
        placement="bottomLeft" >
        <a className="ant-dropdown-link" href="#">
          <Badge count={1} >
            <Avatar shape="square" className="nav-link" icon="user" />
          </Badge>
        </a>
      </Popover>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return { actions: bindActionCreators({
    logout
  }, dispatch) };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserDropdown);
