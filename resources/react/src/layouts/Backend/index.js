import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import { Layout, Menu, Icon, Avatar, Badge, Dropdown, BackTop } from 'antd';
import UserDropdown from './components/UserDropdown';
import ToolbarNotifications from './components/ToolbarNotifications';
import './index.scss';

const { Header, Sider, Content } = Layout;

class Backend extends Component {

  state = {
    collapsed: true
  }

  toggle = () => {
    this.setState({
      collapsed: !this.state.collapsed,
    });
  }

  render() {
    return (
      <Layout id="backend">
        <Sider
          trigger={null}
          collapsible
          collapsed={this.state.collapsed} >
          <div className="logo" />
          <Menu
            selectedKeys={[this.props.currentPath]}
            theme="dark"
            mode="inline" >
            <Menu.Item key="/panel/dashboard">
              <Link to="/panel/dashboard">
                <Icon type="dashboard" />
                <span>Dashboard</span>
              </Link>
            </Menu.Item>
            <Menu.Item key="/panel/artists">
              <Link to="/panel/artists">
                <Icon type="user" />
                <span>Artists</span>
              </Link>
            </Menu.Item>
            <Menu.SubMenu key="/panel/albums" title={<Link to="/panel/albums"><Icon type="folder" /><span>Albums</span></Link>}>
              <Menu.Item key="/panel/album/add">
                <Link to="/panel/album/add">
                  <Icon type="plus" />
                  <span>Add</span>
                </Link>
              </Menu.Item>
            </Menu.SubMenu>
          </Menu>
        </Sider>
        <Layout>
          <Header
            className="header" >
            <Icon
              className="trigger"
              type={this.state.collapsed ? 'menu-unfold' : 'menu-fold'}
              onClick={this.toggle}
            />
            <ul className="menu-right">
              <ToolbarNotifications />
              <li className="nav-item">
                <UserDropdown />
              </li>
            </ul>
          </Header>
          <Content
            className="content">
            {this.props.children}
          </Content>
        </Layout>
        <BackTop />
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    currentPath: state.router.location.pathname
  };
};

const mapDispatchToProps = (dispatch) => {
  return { actions: bindActionCreators({}, dispatch) };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Backend);
