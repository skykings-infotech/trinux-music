import Home from './pages/Frontend/Home';
import Login from './pages/Login';
import NotFound from './pages/Frontend/NotFound';

export {
  Home,
  Login,
  NotFound
}
