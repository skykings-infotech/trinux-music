import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import {
  Row,
  Col,
  Form,
  Input,
  Icon,
  Button
} from 'antd';
import { Backend } from '../../../layouts';

const FormItem = Form.Item;
const { TextArea } = Input;

class AlbumCreate extends Component {

  handleSubmit = ( event ) => {
    event.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.actions.addAlbum(values);
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <Backend id="album">
        <Form onSubmit={this.handleSubmit}>
          <Row>
            <Col
              xxl={{ span: 8 }}
              xl={{ span: 8 }}
              lg={{ span: 8 }}
              span={24} >

            </Col>
            <Col
              xxl={{ span: 16 }}
              xl={{ span: 16 }}
              lg={{ span: 16 }}
              md={{ span: 24 }}
              sm={{ span: 24 }} >
              <Row
                gutter={24} >
                <Col
                  xxl={{ span: 12 }}
                  xl={{ span: 12 }}
                  lg={{ span: 12 }}
                  span={24} >
                  <FormItem
                    label="Title" >
                    {
                      getFieldDecorator('title', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Album Title!',
                          }
                        ],
                      })(
                        <Input
                          prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                          placeholder="Title" />
                      )
                    }
                  </FormItem>
                </Col>
                <Col
                  xxl={{ span: 12 }}
                  xl={{ span: 12 }}
                  lg={{ span: 12 }}
                  md={{ span: 24 }}
                  sm={{ span: 24 }} >
                  <FormItem
                    label="Slug" >
                    {
                      getFieldDecorator('slug', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Album Slug!',
                          }
                        ],
                      })(
                        <Input
                          prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                          placeholder="Slug" />
                      )
                    }
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col
                  span={24}>
                  <FormItem
                    label="Description" >
                    {
                      getFieldDecorator('dsc', {
                        rules: [
                          {
                            required: true,
                            message: 'Please input your Album Description!',
                          }
                        ],
                      })(
                        <TextArea
                          rows={6}
                          prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                          placeholder="Description" />
                      )
                    }
                  </FormItem>
                </Col>
              </Row>
              <Row>
                <Col>
                  <Button
                    type="primary"
                    htmlType="submit"
                    loading={this.props.loading}>Add Album</Button>
                </Col>
              </Row>
            </Col>
          </Row>
        </Form>
      </Backend>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.AlbumsReducer.get( 'loading' )
  };
};

const mapDispatchToProps = (dispatch) => {
  return { actions: bindActionCreators({}, dispatch) };
}

AlbumCreate = connect(
  mapStateToProps,
  mapDispatchToProps
)(AlbumCreate);

export default Form.create()(AlbumCreate);
