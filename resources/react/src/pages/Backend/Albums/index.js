import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { Link } from 'react-router-dom';
import {
  Table,
  Button,
  Popconfirm
} from 'antd';
import { Backend } from '../../../layouts';
import { fetchAlbums, deleteAlbum } from '../../../redux/Album/action';

class Albums extends Component {

  constructor( props ) {
    super( props );
    this.columns = [
      {
        title: '#',
        key: 'index',
        render: (text, record, index) => index + 1,
        width:'50px'
      },
      {
        title: 'ID',
        dataIndex: 'key',
        key: 'id',
        width:'150px'
      },
      {
        title: 'Title',
        dataIndex: 'title',
        key: 'title',
        sorter: true
      },
      {
        title: 'Actions',
        key: 'action',
        render: (text, record, index) => {
          return (
            <Button.Group>
              <Button>Edit</Button>
              <Popconfirm
                title="Are You Sure?"
                onConfirm={() => this.props.actions.deleteAlbum( record.key )}>
                <Button>Delete</Button>
              </Popconfirm>
            </Button.Group>
          );
        },
        width:'150px'
      }
    ];
  }

  componentDidMount() {
    this.props.actions.fetchAlbums();
  }

  handleTableChange = (pagination, filters, sorter) => {
    this.props.actions.fetchAlbums({
      page: pagination.current,
      sort: {
        key: sorter.column.dataIndex,
        sort: sorter.order
      }
    });
  }

  render() {
    return (
      <Backend id="artists">
        <div className="display-flex justify-content-end margin-bottom-5">
          <Link to="/panel/album/add">
            <Button type="primary">Add Album</Button>
          </Link>
        </div>
        <Table
          className="artists-table"
          size="small"
          loading={this.props.loading}
          columns={this.columns}
          dataSource={this.props.albums}
          pagination={this.props.pagination}
          onChange={this.handleTableChange} />
      </Backend>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loading: state.AlbumsReducer.get( 'loading' ),
    albums: state.AlbumsReducer.get( 'albums' ),
    pagination: state.AlbumsReducer.get( 'pagination' )
  };
};

const mapDispatchToProps = (dispatch) => {
  return { actions: bindActionCreators({
    fetchAlbums,
    deleteAlbum
  }, dispatch) };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Albums);
