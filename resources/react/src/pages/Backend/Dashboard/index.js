import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { Button } from 'antd';
import { Backend } from '../../../layouts';

class Dashboard extends Component {
  render() {
    return (
      <Backend id="dashboard">
        <Button>Dashboard</Button>
      </Backend>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return { actions: bindActionCreators({}, dispatch) };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Dashboard);
