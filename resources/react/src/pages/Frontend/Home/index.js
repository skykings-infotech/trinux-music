import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { Frontend } from '../../../layouts';

class Home extends Component {
  render() {
    return (
      <Frontend id="home">
        <button className="button is-info">Home</button>
      </Frontend>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return { actions: bindActionCreators({}, dispatch) };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Home);
