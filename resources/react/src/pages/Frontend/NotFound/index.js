import React, { Component } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";

class NotFound extends Component {
  render() {
    return (
      <div id="not-found">
        NotFound
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {};
};

const mapDispatchToProps = (dispatch) => {
  return { actions: bindActionCreators({}, dispatch) };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NotFound);
