import React, { Component } from 'react';
import { Link, Redirect } from "react-router-dom";
import { bindActionCreators } from 'redux';
import { connect } from "react-redux";
import { login } from '../../redux/Login/action';
import { validate } from '../../redux/Auth/action';
import {
  Layout,
  Row,
  Col,
  Button,
  Form,
  Input,
  Checkbox,
  Icon
} from 'antd';

import './index.scss';

const { Content } = Layout;
const FormItem = Form.Item;

class Login extends Component {

  state = {
    redirectToReferrer: false,
    loging_in: false
  };

  componentWillMount() {
    this.props.actions.validate();
  }

  componentWillReceiveProps(nextProps) {
    if (
      (
        this.props.isLoggedIn !== nextProps.isLoggedIn &&
        nextProps.isLoggedIn
      ) ||
      (
        this.props.isAuthenticated !== nextProps.isAuthenticated &&
        nextProps.isAuthenticated
      )
    ) {
      this.setState({ redirectToReferrer: true });
    }
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.props.form.validateFields((err, values) => {
      if (!err) {
        this.props.actions.login(values);
      }
    });
  }

  render() {
    const { getFieldDecorator } = this.props.form;
    const from = this.props.location.state ? this.props.location.state.from : { pathname: '/panel' };
    const { redirectToReferrer } = this.state;

    if (redirectToReferrer) {
      return <Redirect to={from} />;
    }

    return (
      <Layout id="login">
        <Content>
          <Row>
            <Col
              xxl={{ span: 18 }}
              xl={{ span: 16 }}
              lg={{ span: 16 }}
              span={0} >
              <div className="login-page-title-tagline--wrapper">
                <div className="login-page-title-tagline">
                  <h1 className="title">Trinux Music</h1>
                  <h5 className="tagline">The Music You Lisent</h5>
                </div>
              </div>
            </Col>
            <Col
              xxl={{ span: 6 }}
              xl={{ span: 8 }}
              lg={{ span: 8 }}
              md={{ span: 24 }}
              sm={{ span: 24 }}
              className="login-form-container" >
              <div className="brand brand-text">Trinux music</div>

              <Form onSubmit={this.handleSubmit} className="login-form">
                <FormItem>
                  {
                    getFieldDecorator('email', {
                      rules: [
                        {
                          required: true,
                          message: 'Please input your username!',
                          initialValue: 'mail@gmail.com'
                        }
                      ],
                    })(
                      <Input
                        className="transparent-input"
                        prefix={<Icon type="user" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        placeholder="Email" />
                    )
                  }
                </FormItem>
                <FormItem>
                  {
                    getFieldDecorator('password', {
                      rules: [
                        {
                          required: true,
                          message: 'Please input your Password!',
                          initialValue: 'pass'
                        }
                      ],
                    })(
                      <Input
                        className="transparent-input"
                        prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                        type="password"
                        placeholder="Password" />
                    )
                  }
                </FormItem>
                <FormItem className="login-buttons">
                  {
                    getFieldDecorator('remember', {
                      valuePropName: 'checked',
                      initialValue: true,
                    }) (
                      <Checkbox>Remember me</Checkbox>
                    )
                  }
                  <Button
                    type="primary"
                    htmlType="submit"
                    loading={this.props.loging_in}
                    className="login-form-button">LogIn</Button>
                </FormItem>
              </Form>
            </Col>
          </Row>
        </Content>
      </Layout>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    loging_in: state.loginReducer.get( 'loading' ),
    isLoggedIn: state.loginReducer.get( 'isLoggedIn' ),
    isAuthenticated: state.AuthReducer.get( 'authenticated' )
  };
};

const mapDispatchToProps = (dispatch) => {
  return { actions: bindActionCreators({
    login,
    validate
  }, dispatch) };
}

Login = connect(
  mapStateToProps,
  mapDispatchToProps
)(Login);

export default Form.create()(Login);
