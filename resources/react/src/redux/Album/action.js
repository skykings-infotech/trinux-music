export const Actions = {
  FETCH_ALBUMS_REQUEST: 'FETCH_ALBUMS_REQUEST',
  FETCH_ALBUMS_SUCCESS: 'FETCH_ALBUMS_SUCCESS',
  FETCH_ALBUMS_ERROR: 'FETCH_ALBUMS_ERROR',
  DELETE_ALBUM_REQUEST: 'DELETE_ALBUM_REQUEST',
  DELETE_ALBUM_SUCCESS: 'DELETE_ALBUM_SUCCESS',
  DELETE_ALBUM_ERROR: 'DELETE_ALBUM_ERROR'
};

export function fetchAlbums( params ) {
  return {
    type: Actions.FETCH_ALBUMS_REQUEST,
    payload: {
      params
    }
  };
}

export function deleteAlbum( id ) {
  return {
    type: Actions.DELETE_ALBUM_REQUEST,
    payload: {
      id: id
    }
  };
}
