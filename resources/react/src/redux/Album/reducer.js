import { Map } from "immutable";
import { Actions } from './action';

const initState = new Map({
  loading: false,
  albums: []
});

export default function AlbumsReducer(state = initState, action) {

  switch (action.type) {
    case Actions.FETCH_ALBUMS_REQUEST:
      state = state.set('loading', true);
      return state;
    case Actions.FETCH_ALBUMS_SUCCESS:
      state = state.set('pagination', {
        current: action.payload.pagination.current,
        pageSize: action.payload.pagination.limit,
        total: action.payload.pagination.total_albums,
        offset: action.payload.pagination.offset,
        totalPages: action.payload.pagination.total
      });
      state = state.set('albums', action.payload.albums);
      state = state.set('loading', false);
      return state;
    case Actions.FETCH_ALBUMS_ERROR:
      state = state.set('loading', false);
      return state;
    default:
      return state;
  }

}
