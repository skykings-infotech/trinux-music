import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { Http } from '../../helpers';
import { Actions } from './action';

export function* fetchAlbumsRequest() {
  yield takeEvery(Actions.FETCH_ALBUMS_REQUEST, function*( action ) {
    const response = yield Http.get( 'albums', action.payload );
    if( response.statusText === 'OK' ) {
      return yield put({ type: Actions.FETCH_ALBUMS_SUCCESS, payload: response.data });
    }
    yield put({ type: Actions.FETCH_ALBUMS_ERROR });
  });
}

export function* deleteAlbumsRequest() {
  yield takeEvery(Actions.DELETE_ALBUM_REQUEST, function*( action ) {
    const response = yield Http.delete( 'album', action.payload );
    if( response.data.staus ) {
      return yield put({ type: Actions.DELETE_ALBUM_SUCCESS });
    }
    yield put({ type: Actions.DELETE_ALBUM_ERROR });
  });
}

export default function* rootSaga() {
  yield all([
    fork(fetchAlbumsRequest),
    fork(deleteAlbumsRequest)
  ]);
}
