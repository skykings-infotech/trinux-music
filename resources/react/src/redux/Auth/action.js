export const Actions = {
  AUTH_REQUEST: 'AUTH_REQUEST',
  AUTH_SUCCESS: 'AUTH_SUCCESS',
  AUTH_ERROR: 'AUTH_ERROR',
  AUTH_DESTROY: 'AUTH_DESTROY',
  LOGOUT_REQUEST: 'LOGOUT_REQUEST',
  LOGOUT_SUCCESS: 'LOGOUT_SUCCESS',
  LOGOUT_ERROR: 'LOGOUT_ERROR'
};

export function validate() {
  return {
    type: Actions.AUTH_REQUEST
  };
}

export function logout() {
  return {
    type: Actions.LOGOUT_REQUEST
  }
}
