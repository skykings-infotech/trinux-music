import { Map } from "immutable";
import { Actions } from './action';

const initState = new Map({
  authenticated: false
});

export default function AuthReducer(state = initState, action) {

  switch (action.type) {
    case Actions.AUTH_SUCCESS:
      state = state.set('authenticated', true);
      state = state.set('user', action.user);
      return state;
    case Actions.AUTH_DESTROY:
      return initState;
    default:
      return state;
  }

}
