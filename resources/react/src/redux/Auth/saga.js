import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { Http } from '../../helpers';
import { Actions } from './action';
import { Actions as LoginActions } from '../Login/action';

export function* validateAuthRequest() {
  yield takeEvery(Actions.AUTH_REQUEST, function*( action ) {
    const response = yield Http.get( 'user', action.payload );
    if( response.data.status ) {
      yield put({ type: LoginActions.LOGIN_SUCCESS, user: response.data.user });
      return yield put({ type: Actions.AUTH_SUCCESS, user: response.data.user });
    }
    yield put({ type: Actions.AUTH_ERROR });
  });
}

export function* logoutUser() {
  yield takeEvery(Actions.LOGOUT_REQUEST, function*( action ) {
    const response = yield Http.post( 'logout', action.payload );
    if( response.data.status ) {
      yield put({ type: LoginActions.LOGIN_DESTROY });
      yield put({ type: Actions.AUTH_DESTROY });
      yield put({ type: Actions.LOGOUT_SUCCESS });
      return yield put( push( '/auth/login' ) );
    }
    yield put({ type: Actions.LOGOUT_ERROR });
  });
}

export default function* rootSaga() {
  yield all([
    fork(validateAuthRequest),
    fork(logoutUser)
  ]);
}
