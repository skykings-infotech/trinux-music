export const Actions = {
  LOGIN_REQUEST: 'LOGIN_REQUEST',
  LOGIN_SUCCESS: 'LOGIN_SUCCESS',
  LOGIN_ERROR: 'LOGIN_ERROR',
  LOGIN_DESTROY: 'LOGIN_DESTROY'
};

export function login(creds = {}) {
  return {
    type: Actions.LOGIN_REQUEST,
    payload: creds
  };
}
