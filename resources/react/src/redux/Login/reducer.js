import { Map } from "immutable";
import { Actions } from './action';

const initState = new Map({
  isLoggedIn: false,
  loading: false
});

export default function loginReducer(state = initState, action) {

  switch (action.type) {
    case Actions.LOGIN_REQUEST:
      state = state.set('loading', true);
      return state;
    case Actions.LOGIN_SUCCESS:
      state = state.set('isLoggedIn', true);
      state = state.set('user', action.user);
      state = state.set('loading', false);
      return state;
    case Actions.LOGIN_ERROR:
      state = state.set('loading', false);
      return state;
    case Actions.LOGIN_DESTROY:
      return initState;
    default:
      return state;
  }

}
