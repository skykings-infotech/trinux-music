import { all, takeEvery, put, fork, call } from 'redux-saga/effects';
import { push } from 'react-router-redux';
import { Http } from '../../helpers';
import { Actions } from './action';

export function* loginRequest() {
  yield takeEvery(Actions.LOGIN_REQUEST, function*( action ) {
    const response = yield Http.post( 'login', action.payload );
    if( response.data.status ) {
      return yield put({ type: Actions.LOGIN_SUCCESS, user: response.data.user });
    }
    yield put({ type: Actions.LOGIN_ERROR });
  });
}

export default function* rootSaga() {
  yield all([
    fork(loginRequest)
  ]);
}
