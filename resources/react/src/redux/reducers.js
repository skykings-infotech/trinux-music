import loginReducer from './Login/reducer';
import AuthReducer from './Auth/reducer';
import AlbumsReducer from './Album/reducer';

export default {
  loginReducer,
  AuthReducer,
  AlbumsReducer
};
