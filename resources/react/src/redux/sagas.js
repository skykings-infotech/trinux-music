import { all } from 'redux-saga/effects';
import loginSaga from './Login/saga';
import authSaga from './Auth/saga';
import albumSaga from './Album/saga';

export default function* mainSaga(getState) {
  yield all([
    loginSaga(),
    authSaga(),
    albumSaga()
  ]);
}
