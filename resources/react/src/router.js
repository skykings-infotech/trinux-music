import React from 'react';
import { Route, Redirect, Switch } from 'react-router';
import { ConnectedRouter } from 'react-router-redux';
import { connect } from 'react-redux';
import routes from './routes';

function createRoutes(routes, restricted = false, isLoggedIn = false) {
  return routes.map((route) => {

    if(typeof route.redirectTo != 'undefined') {
      return <Route
        exact={route.exact}
        path={route.path}
        key={route.path}
        component={() => (
          <Redirect to={route.redirectTo}/>
        )} />
    }

    if(restricted) {
      return <RestrictedRoute
        exact={route.exact}
        path={route.path}
        key={route.path}
        component={route.component}
        isLoggedIn={isLoggedIn} />
    } else {
      return <Route
        exact={route.exact}
        path={route.path}
        key={route.path}
        component={route.component} />
    }
  });
}

const RestrictedRoute = ({ component: Component, isLoggedIn, ...rest }) => (
  <Route
    {...rest}
    render={props => isLoggedIn
      ? <Component {...props} />
      : <Redirect
          to={{
            pathname: '/auth/login',
            state: { from: props.location },
          }}
        />}
  />
);

const PublicRoutes = ({ history, isLoggedIn }) => {
  return (
    <ConnectedRouter history={history}>
      <Switch>
      {createRoutes(routes.public, false)}
      {createRoutes(routes.protected, true, isLoggedIn)}
      <Redirect to='/404' />
      </Switch>
    </ConnectedRouter>
  );
};

export default connect(state => ({
  isLoggedIn: state.loginReducer.get('isLoggedIn')
}))(PublicRoutes);
