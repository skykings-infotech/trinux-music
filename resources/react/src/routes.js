import {
  Home,
  Login,
  NotFound
} from './pages';

import {
  Dashboard,
  Artists,
  Albums,
  AlbumCreate
} from './admin-pages';

export default {
  public: [
    {
      path: '/',
      component: Home,
      exact: true
    },
    {
      path: '/auth',
      redirectTo: '/auth/login',
      exact: true
    },
    {
      path: '/auth/login',
      component: Login,
      exact: true
    },
    {
      path: '/404',
      component: NotFound,
      exact: true
    }
  ],
  protected: [
    {
      path: '/panel',
      redirectTo: '/panel/dashboard',
      exact: true
    },
    {
      path: '/panel/dashboard',
      component: Dashboard,
      exact: true
    },
    {
      path: '/panel/artists',
      component: Artists,
      exact: true
    },
    {
      path: '/panel/albums',
      component: Albums,
      exact: true
    },
    {
      path: '/panel/album/add',
      component: AlbumCreate,
      exact: true
    }
  ]
};
