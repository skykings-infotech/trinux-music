<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('{slug}', function () {
    return view('index');
})->where('slug', '(?!(api|ajax))([A-z\d-\/_.]+)?');

Route::group([ 'prefix' => '/ajax' ], function() {

  // Authentication Routes
  Route::post('/login', 'AuthController@login');
  Route::post('/logout', 'AuthController@logout');
  Route::get('/user', 'AuthController@user');

  // Album Routes
  Route::get( '/albums', 'AlbumsController@getAll' );
  Route::group( [ 'prefix' => '/album' ], function () {
    Route::get( '/{id}', 'AlbumsController@get' );
    Route::post( '/', 'AlbumsController@add' );
    Route::put( '/{id}', 'AlbumsController@update' );
    Route::delete( '/{id}', 'AlbumsController@delete' );
  } );

  // Artist Routes
  Route::get( '/artists', 'ArtistsController@getAll' );
  Route::group( [ 'prefix' => '/artist' ], function () {
    Route::get( '/{id}', 'ArtistsController@get' );
    Route::post( '/', 'ArtistsController@add' );
    Route::put( '/{id}', 'ArtistsController@update' );
    Route::delete( '/{id}', 'ArtistsController@delete' );
  } );
});

Route::get( '/ajax/lost', function() {
  $user = new App\User();
  $user->password = Hash::make('pass');
  $user->name = 'Rishabh';
  $user->email = 'email@example.com';
  $user->save();
} );
